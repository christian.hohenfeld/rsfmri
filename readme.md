# The rsfmri R package
This package provides lower level tools to work with MRI data from within R,
the focus being resting-state functional MRI data.
It provides wrappers around tools from FSL and AFNI, tools for handling
physiological records stored in DICOM files and convenience tools for common
tasks such as skull stripping. 

This package has been created in the research group _Imaging in
Neurodegenerative Diseases_ at the University Hospital Aachen, Germany.

## Installation
It should be sufficient to call `devtools::install_gitlab("choh/rsfmri")`, as
all dependencies are available on CRAN. 
You should probably also at least install [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/). 
Note that this package relies quite a bit on calls to tools outside of R and
has been developed with Unix-like systems (Linux, MacOS, BSD, etc.) in mind. 

## Licence 
This package is available under the GNU GPL v3.
