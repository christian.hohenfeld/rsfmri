is_fsl_installed <- function(fslpath = NULL, summary = TRUE) {
#' Check if FSL is available.
#'
#' Tries invoking the needed tools from FSL from inside R. If this fails you
#' probably need to install the FSL toolset or if you have it already installed
#' in your system add it to you path where R can find it.
#'
#' @param fslpath Optinal a character vector specifying a path where to look
#' for the AFNI tools.
#' @param summary If TRUE return just a single bool indicating whether
#' everything was found. If FALSE returns a detailed named list giving
#' information on what was found and what could not be located.
#'
#' @return Bools inidcating whether FSL tool were found.
#'
#' @details If nothing can be found you probably want to edit the .Renviron
#' file to inlucde the path to the FSL binaries.
#'
#' @export
    cmdnames <- c(mcflirt = "mcflirt",
                  fslmaths = "fslmaths",
                  bet = "bet",
                  flirt = "flirt",
                  fslmaths = "fslmaths",
                  fslmeants = "fslmeants",
                  fugue = "fugue",
                  slicetimer = "slicetimer")
    if (!is.null(fslpath)) {
        cmdnames <- file.path(fslpath, cmdnames)
    }

    fsl_list <- list()
    fsl_list <- sapply(cmdnames, .which, USE.NAMES = TRUE, simplify = FALSE)

    if (summary) {
        fsl_list <- unlist(fsl_list)
        fsl_check <- all(fsl_list)
    } else {
        fsl_check <- fsl_list
    }

    fsl_check
}
