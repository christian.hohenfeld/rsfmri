fsl_reorient2std <- function(infile, outfile = NULL, debug = FALSE) {
#' Reorient an image into standard directions.
#'
#' @param infile Path to the input file. This can not be omitted.
#' @param outfile Path to the outfile. Optional.
#' @param debug Bool. If true command line output is not supressed.
#'
#' @return The file name of the expected new file and two attributes giving
#' the return code and wheter the file does in fact exist.
#'
#' @export
    infile <- normalizePath(infile)

    if (is.null(outfile)) {
        new_file <- .infix(infile, "reori")
    } else {
        new_file <- outfile
    }

    args_vector <- c(infile, new_file)

    r <- system2("fslreorient2std", args = args_vector,
                 stdout = debug, stderr = debug,
                 env = "FSLOUTPUTTYPE=NIFTI_GZ")

    .returnobject(new_file, r)
}
