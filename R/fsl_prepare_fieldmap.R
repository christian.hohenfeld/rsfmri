#' Generate a fieldmap for EPI correction.
#'
#' @param magnitude_map Path to the "magnitude map".
#' @param phase_map Path to the "phase map".
#' @param outfile Optional path to write to.
#' @param echo_diff The echo spacing of the fieldmaps in milliseconds.
#'
#' @export
fsl_prepare_fieldmap <- function(magnitude_map, phase_map,
                                 outfile = NULL, echo_diff) {
    in_mag <- normalizePath(magnitude_map)
    in_pha <- normalizePath(phase_map)

    if (is.null(outfile)) {
        mag_base <- basename(in_mag)
        parent_dir <- gsub(paste0("/", mag_base, "$"), "", in_mag)
        new_file <- file.path(parent_dir, "fmap.nii.gz")
    } else {
        new_file <- outfile
    }

    args_vector <- c("SIEMENS", in_pha, in_mag, new_file,
                     as.character(echo_diff))
    r <- system2("fsl_prepare_fieldmap", args_vector, stdout = FALSE,
                 stderr = FALSE, env = "FSLOUTPUTTYPE=NIFTI_GZ")
    .returnobject(new_file, r)
}
